package com.app.unidad1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_RECEIVE_SMS = 0;
    Button actu, ganador, rei;
    String t = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        actu = findViewById(R.id.actualizar);
        ganador = findViewById(R.id.elegir);
        rei = findViewById(R.id.reinicia);

        if(ContextCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)!= PackageManager.PERMISSION_GRANTED){
            if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.RECEIVE_SMS)){

            }else{
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECEIVE_SMS}, MY_PERMISSIONS_REQUEST_RECEIVE_SMS);
            }
        }

        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.SEND_SMS}, 1);
        }

        mostrar();

        actu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrar();
            }
        });

        ganador.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gana();
            }
        });

        rei.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reiniciar();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResuts){
        switch(requestCode){
            case MY_PERMISSIONS_REQUEST_RECEIVE_SMS:{
                if(grantResuts.length > 0 && grantResuts[0] == PackageManager.PERMISSION_GRANTED){
                    Toast.makeText(this, "Thacksyou for permitting!", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this, "Well I can't do anything until you permit me", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    public void mostrar(){
        AdminBD dbHelper = new AdminBD(this, "sorteo6", null, 1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        ListView list = (ListView) findViewById(R.id.lista);

        Cursor c = db.rawQuery("SELECT boleto, telefono FROM sorteo6", null);

        ArrayList<String> lista = new ArrayList<>();
        if(c.moveToFirst()){
            do {
                String id = c.getString(0);
                String tel = c.getString(1);
                lista.add(id + " - " + tel);
            }while (c.moveToNext());
        }
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        list.setAdapter(adaptador);
    }

    public void gana(){
        AdminBD dbHelper = new AdminBD(this, "sorteo6", null, 1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        Cursor c = db.rawQuery("SELECT boleto, telefono FROM sorteo6 ORDER BY RANDOM() LIMIT 1", null);
        String id = "";
        String tel = "";

        if(c.moveToFirst()){
            do {
                id = c.getString(0);
                tel = c.getString(1);
            }while (c.moveToNext());
        }
        //t = tel.substring(2);
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(tel, null, "Felizidades fuiste el ganador con el boleto: " + id, null, null);
        Toast.makeText(this, "El ganador es: " + tel + "\nCon el boleto: " + id, Toast.LENGTH_SHORT).show();
    }

    public void reiniciar(){
        AdminBD dbHelper = new AdminBD(this, "sorteo6", null, 1);
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.delete("sorteo6", null, null);
    }
}
