package com.app.unidad1;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

public class MyReceiver extends BroadcastReceiver {

    private static final String SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private static final String TAG = "SmsBroadcastReceiver";
    String msg, phoneNo = "";
    String bo = "0";
    int bole;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "Intent Received: " + intent.getAction());
        if (intent.getAction().equals(SMS_RECEIVED)){
            Bundle dataBundle = intent.getExtras();
            if (dataBundle != null){
                Object[] mypdu = (Object[])dataBundle.get("pdus");
                SmsMessage[] message = Telephony.Sms.Intents.getMessagesFromIntent(intent);
                for (int i = 0; i < mypdu.length; i++){
                    message[i] = SmsMessage.createFromPdu((byte[])mypdu[i]);
                    msg = message[i].getMessageBody();
                    phoneNo = message[i].getOriginatingAddress();
                }
                //Toast.makeText(context, "Mensaje: " + msg + "\nNumero: " + phoneNo, Toast.LENGTH_LONG).show();
            }
        }
        if(msg.contentEquals("participar")){
            AdminBD admin = new AdminBD(context, "sorteo6", null, 1);
            SQLiteDatabase BaseDatos = admin.getWritableDatabase();
            Cursor c = BaseDatos.rawQuery("SELECT boleto, telefono FROM sorteo6", null);

            if(c.moveToFirst()){
                do {
                    bo = c.getString(0);
                }while (c.moveToNext());
            }
            bole = Integer.parseInt(bo);

            bole = bole + 1;
            ContentValues registro = new ContentValues();
            registro.put("boleto", bole);
            registro.put("telefono", phoneNo);
            BaseDatos.insert("sorteo6", null, registro);
            BaseDatos.close();

            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(phoneNo, null, "Boleto: " + bole, null, null);

            Toast.makeText(context, "Almacenando Datos", Toast.LENGTH_SHORT).show();
            //Toast.makeText(context, "Boleto enviado", Toast.LENGTH_LONG).show();
        }
    }
}
